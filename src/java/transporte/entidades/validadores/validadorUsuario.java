/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.EmpresaFacadeREST;
import co.gov.transporte.controlador.servicios.RutaFacadeREST;
import co.gov.transporte.controlador.servicios.UsuarioFacadeREST;
import co.gov.transporte.entidades.Empresa;
import co.gov.transporte.entidades.Ruta;
import co.gov.transporte.entidades.Usuario;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorUsuario {
    
     public static void validarEmailExistente(String email, UsuarioFacadeREST usuarioFacade) 
            throws EntidadesException
    {
        if(usuarioFacade.find(email)!=null)
        {
            throw new EntidadesException("El e-mail ya existe");
        }
    }
    
     public static void validarEmailNoExistente(String email, UsuarioFacadeREST usuarioFacade) 
            throws EntidadesException
    {
        if(usuarioFacade.find(email)!=null)
        {
            throw new EntidadesException("El e-mail ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Usuario usuario) 
            throws EntidadesException
    {
        if(usuario.getEmail()==null || usuario.getEmail().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar el e-mail del usuario");
        }
    
    
    }
    
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
}
