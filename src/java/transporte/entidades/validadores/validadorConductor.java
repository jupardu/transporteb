/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.ConductorFacadeREST;
import co.gov.transporte.entidades.Conductor;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorConductor {
    
     public static void validarConductorExistente(String cedula, ConductorFacadeREST conductorFacade) 
            throws EntidadesException
    {
        if(conductorFacade.find(cedula)!=null)
        {
            throw new EntidadesException("El conductor ya existe");
        }
    }
    
     public static void validarConductorNoExistente(String cedula, ConductorFacadeREST conductorFacade) 
            throws EntidadesException
    {
        if(conductorFacade.find(cedula)!=null)
        {
            throw new EntidadesException("El conductor ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Conductor conductor) 
            throws EntidadesException
    {
        if(conductor.getCedula()==null || conductor.getCedula().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar la cédula del conductor");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
    
    
}
