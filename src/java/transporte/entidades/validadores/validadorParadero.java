/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.EmpresaFacadeREST;
import co.gov.transporte.controlador.servicios.ParaderoFacadeREST;
import co.gov.transporte.entidades.Empresa;
import co.gov.transporte.entidades.Paradero;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorParadero {
    
     public static void validarIdExistente(String id, ParaderoFacadeREST paraderoFacade) 
            throws EntidadesException
    {
        if(paraderoFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ya existe");
        }
    }
    
     public static void validarIdNoExistente(String id, ParaderoFacadeREST paraderoFacade) 
            throws EntidadesException
    {
        if(paraderoFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Paradero paradero) 
            throws EntidadesException
    {
        if(paradero.getId()==null || paradero.getId().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar el id del paradero");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
}
