/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.ConductorFacadeREST;
import co.gov.transporte.controlador.servicios.EmpresaFacadeREST;
import co.gov.transporte.entidades.Conductor;
import co.gov.transporte.entidades.Empresa;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorEmpresa {
    
     public static void validarNitExistente(String nit, EmpresaFacadeREST empresaFacade) 
            throws EntidadesException
    {
        if(empresaFacade.find(nit)!=null)
        {
            throw new EntidadesException("El nit ya existe");
        }
    }
    
     public static void validarNitNoExistente(String nit, EmpresaFacadeREST empresaFacade) 
            throws EntidadesException
    {
        if(empresaFacade.find(nit)!=null)
        {
            throw new EntidadesException("El nit ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Empresa empresa) 
            throws EntidadesException
    {
        if(empresa.getNit()==null || empresa.getNit().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar el nit de la empresa");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
}
