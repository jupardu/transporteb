/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.RutaFacadeREST;
import co.gov.transporte.entidades.Ruta;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorRuta {
    
     public static void validarIdExistente(String id, RutaFacadeREST rutaFacade) 
            throws EntidadesException
    {
        if(rutaFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ya existe");
        }
    }
    
     public static void validarIdNoExistente(String id, RutaFacadeREST rutaFacade) 
            throws EntidadesException
    {
        if(rutaFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Ruta ruta) 
            throws EntidadesException
    {
        if(ruta.getId()==null || ruta.getId().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar el id de la ruta");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
}
