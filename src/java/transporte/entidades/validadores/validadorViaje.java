/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.EmpresaFacadeREST;
import co.gov.transporte.controlador.servicios.RutaFacadeREST;
import co.gov.transporte.controlador.servicios.UsuarioFacadeREST;
import co.gov.transporte.controlador.servicios.ViajeFacadeREST;
import co.gov.transporte.entidades.Empresa;
import co.gov.transporte.entidades.Ruta;
import co.gov.transporte.entidades.Usuario;
import co.gov.transporte.entidades.Viaje;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class validadorViaje {
    
     public static void validarIdExistente(String id, ViajeFacadeREST viajeFacade) 
            throws EntidadesException
    {
        if(viajeFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ya existe");
        }
    }
    
     public static void validarIdNoExistente(String id, ViajeFacadeREST viajeFacade) 
            throws EntidadesException
    {
        if(viajeFacade.find(id)!=null)
        {
            throw new EntidadesException("El id ingresado no existe");
        }
    }
    
    public static void validarCamposObligatorios(Viaje viaje) 
            throws EntidadesException
    {
        if(viaje.getId()==null || viaje.getId().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar el id del viaje");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
    
}
