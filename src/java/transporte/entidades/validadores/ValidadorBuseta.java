/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.entidades.validadores;

import co.gov.transporte.controlador.servicios.BusetaFacadeREST;
import co.gov.transporte.entidades.Buseta;
import transporte.entidades.excepciones.EntidadesException;

/**
 *
 * @author JULIAN
 */
public class ValidadorBuseta {
    
    public static void validarPlacaExistente(String placa, BusetaFacadeREST busetaFacade) 
            throws EntidadesException
    {
        if(busetaFacade.find(placa)!=null)
        {
            throw new EntidadesException("la placa ingresada ya existe");
        }
    }
    
     public static void validarPlacaNoExistente(String placa, BusetaFacadeREST busetaFacade) 
            throws EntidadesException
    {
        if(busetaFacade.find(placa)!=null)
        {
            throw new EntidadesException("La placa ingresada no existe");
        }
    }
    
    public static void validarCamposObligatorios(Buseta buseta) 
            throws EntidadesException
    {
        if(buseta.getPlaca()==null || buseta.getPlaca().equals(""))
        {
            throw new EntidadesException("Debe dilingenciar la placa de la buseta");
        }
    
    
    }
    
    public static void validarAutenticacion(String auth, String passwd)
            throws EntidadesException
    {
        if(!auth.equals("julian") || !passwd.equals("seminario"))
        {
            throw new EntidadesException("Usted no tiene permiso para acceder a este recurso");
        }
    }
}
