/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transporte.beans;
import co.gov.transporte.controlador.UsuarioFacade;
import co.gov.transporte.entidades.Usuario;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletContext;
import javax.inject.Named;


/**
 *
 * @author Admin
 */
@Named(value = "loginBean")
@SessionScoped
//@RequestScoped
public class LoginBean implements Serializable {
    
private String email="";
private String clave="";

@EJB 
//private UsuarioFacade connFacade;
private UsuarioFacade usuarioFacade;

    public LoginBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String autenticar() throws NoSuchAlgorithmException, Exception{
        
        Usuario usuarioAutenticado = usuarioFacade.obtenerxTipoUsuario(email);
       
        if (usuarioAutenticado != null) {
            
            //String userContrasenaIngresada = Encriptar.getStringMessageDigest(password, Encriptar.MD5);
            
            if (clave.equals(usuarioAutenticado.getClave())) {
                return "ingresar";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Clave invalida"));
                return null;
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Correo invalido"));
        return null;
    }

}
