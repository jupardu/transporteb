/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Juan
 */
@Entity
@Table(name = "viaje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Viaje.findAll", query = "SELECT v FROM Viaje v")
    , @NamedQuery(name = "Viaje.findById", query = "SELECT v FROM Viaje v WHERE v.id = :id")
    , @NamedQuery(name = "Viaje.findByHorasalida", query = "SELECT v FROM Viaje v WHERE v.horasalida = :horasalida")
    , @NamedQuery(name = "Viaje.findByHorallegada", query = "SELECT v FROM Viaje v WHERE v.horallegada = :horallegada")})
public class Viaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "id")
    private String id;
    @Column(name = "horasalida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horasalida;
    @Column(name = "horallegada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horallegada;
    @JoinColumn(name = "placabuseta", referencedColumnName = "placa")
    @ManyToOne
    private Buseta placabuseta;
    @JoinColumn(name = "idparaderoactual", referencedColumnName = "id")
    @ManyToOne
    private Paradero idparaderoactual;

    public Viaje() {
    }

    public Viaje(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getHorasalida() {
        return horasalida;
    }

    public void setHorasalida(Date horasalida) {
        this.horasalida = horasalida;
    }

    public Date getHorallegada() {
        return horallegada;
    }

    public void setHorallegada(Date horallegada) {
        this.horallegada = horallegada;
    }

    public Buseta getPlacabuseta() {
        return placabuseta;
    }

    public void setPlacabuseta(Buseta placabuseta) {
        this.placabuseta = placabuseta;
    }

    public Paradero getIdparaderoactual() {
        return idparaderoactual;
    }

    public void setIdparaderoactual(Paradero idparaderoactual) {
        this.idparaderoactual = idparaderoactual;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viaje)) {
            return false;
        }
        Viaje other = (Viaje) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.transporte.entidades.Viaje[ id=" + id + " ]";
        
    }
    
}
