/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juan
 */
@Entity
@Table(name = "buseta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Buseta.findAll", query = "SELECT b FROM Buseta b")
    , @NamedQuery(name = "Buseta.findByPlaca", query = "SELECT b FROM Buseta b WHERE b.placa = :placa")
    , @NamedQuery(name = "Buseta.findByPlacalateral", query = "SELECT b FROM Buseta b WHERE b.placalateral = :placalateral")})
public class Buseta implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "floatlatitud")
    private double floatlatitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "floatlongitud")
    private double floatlongitud;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "placa")
    private String placa;
    @Size(max = 10)
    @Column(name = "placalateral")
    private String placalateral;
    @JoinColumn(name = "cedulaconductor", referencedColumnName = "cedula")
    @ManyToOne
    private Conductor cedulaconductor;
    @JoinColumn(name = "nitempresa", referencedColumnName = "nit")
    @ManyToOne
    private Empresa nitempresa;
    @OneToMany(mappedBy = "placabuseta")
    private List<Viaje> viajeList;

    public Buseta() {
    }

    public Buseta(String placa) {
        this.placa = placa;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPlacalateral() {
        return placalateral;
    }

    public void setPlacalateral(String placalateral) {
        this.placalateral = placalateral;
    }

    public Conductor getCedulaconductor() {
        return cedulaconductor;
    }

    public void setCedulaconductor(Conductor cedulaconductor) {
        this.cedulaconductor = cedulaconductor;
    }

    public Empresa getNitempresa() {
        return nitempresa;
    }

    public void setNitempresa(Empresa nitempresa) {
        this.nitempresa = nitempresa;
    }

    @XmlTransient
    public List<Viaje> getViajeList() {
        return viajeList;
    }

    public void setViajeList(List<Viaje> viajeList) {
        this.viajeList = viajeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (placa != null ? placa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buseta)) {
            return false;
        }
        Buseta other = (Buseta) object;
        return !((this.placa == null && other.placa != null) || (this.placa != null && !this.placa.equals(other.placa)));
    }

    @Override
    public String toString() {
        //return "co.gov.transporte.entidades.Buseta[ placa=" + placa + " ]";
        return this.getPlaca();
    }

    public double getFloatlatitud() {
        return floatlatitud;
    }

    public void setFloatlatitud(double floatlatitud) {
        this.floatlatitud = floatlatitud;
    }

    public double getFloatlongitud() {
        return floatlongitud;
    }

    public void setFloatlongitud(double floatlongitud) {
        this.floatlongitud = floatlongitud;
    }

    public String getNombreBuseta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
