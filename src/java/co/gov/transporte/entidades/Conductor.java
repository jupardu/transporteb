/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Juan
 */
@Entity
@Table(name = "conductor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conductor.findAll", query = "SELECT c FROM Conductor c")
    , @NamedQuery(name = "Conductor.findByCedula", query = "SELECT c FROM Conductor c WHERE c.cedula = :cedula")
    , @NamedQuery(name = "Conductor.findByNombre", query = "SELECT c FROM Conductor c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Conductor.findByApellidos", query = "SELECT c FROM Conductor c WHERE c.apellidos = :apellidos")
    , @NamedQuery(name = "Conductor.findByGruposanguineo", query = "SELECT c FROM Conductor c WHERE c.gruposanguineo = :gruposanguineo")
    , @NamedQuery(name = "Conductor.findByEps", query = "SELECT c FROM Conductor c WHERE c.eps = :eps")})
public class Conductor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "cedula")
    private String cedula;
    @Size(max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 50)
    @Column(name = "apellidos")
    private String apellidos;
    @Size(max = 3)
    @Column(name = "gruposanguineo")
    private String gruposanguineo;
    @Size(max = 50)
    @Column(name = "eps")
    private String eps;
    @OneToMany(mappedBy = "cedulaconductor")
    private List<Buseta> busetaList;
    @JoinColumn(name = "nitempresa", referencedColumnName = "nit")
    @ManyToOne
    private Empresa nitempresa;

    public Conductor() {
    }

    public Conductor(String cedula) {
        this.cedula = cedula;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGruposanguineo() {
        return gruposanguineo;
    }

    public void setGruposanguineo(String gruposanguineo) {
        this.gruposanguineo = gruposanguineo;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    @XmlTransient
    public List<Buseta> getBusetaList() {
        return busetaList;
    }

    public void setBusetaList(List<Buseta> busetaList) {
        this.busetaList = busetaList;
    }

    public Empresa getNitempresa() {
        return nitempresa;
    }

    public void setNitempresa(Empresa nitempresa) {
        this.nitempresa = nitempresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cedula != null ? cedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conductor)) {
            return false;
        }
        Conductor other = (Conductor) object;
        if ((this.cedula == null && other.cedula != null) || (this.cedula != null && !this.cedula.equals(other.cedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "co.gov.transporte.entidades.Conductor[ cedula=" + cedula + " ]";
        return (this.getNombre()+ " " + this.getApellidos());
    }
    
}
