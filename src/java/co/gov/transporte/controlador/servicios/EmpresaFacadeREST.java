/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.controlador.servicios;

import co.gov.transporte.entidades.Empresa;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import transporte.dto.respuestaDto;
import transporte.entidades.excepciones.EntidadesException;
import transporte.entidades.validadores.ValidadorBuseta;
import transporte.entidades.validadores.validadorConductor;
import transporte.entidades.validadores.validadorEmpresa;

/**
 *
 * @author JULIAN
 */
@Stateless
@Path("co.gov.transporte.entidades.empresa")
public class EmpresaFacadeREST extends AbstractFacade<Empresa> {

    @PersistenceContext(unitName = "transporteBPU")
    private EntityManager em;

    public EmpresaFacadeREST() {
        super(Empresa.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto createEmpresa(Empresa entity, @HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password ) {
        try{
          validadorEmpresa.validarAutenticacion(authentication, password);
          validadorEmpresa.validarNitExistente(entity.getNit(), this);
          validadorEmpresa.validarCamposObligatorios(entity);
            super.create(entity);
          return new respuestaDto("406","La empresa fue creada con exito");        
        }catch (EntidadesException ex){
          return new respuestaDto("406", ex.getMessage());
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto editEmpresa(Empresa entity) {
        
        try{
            validadorEmpresa.validarNitNoExistente(entity.getNit(), this);
            validadorEmpresa.validarCamposObligatorios(entity);        
        super.edit(entity);
        return new respuestaDto("204","Empresa actualizada correctamente");
        } catch (EntidadesException ex) {
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @DELETE
    @Path("{nit}")
    public respuestaDto removeEmpresa(@PathParam("nit") String nit) {
        try{
            validadorEmpresa.validarNitExistente(nit, this);
            super.remove(super.find(nit));
            return new respuestaDto("204","Empresa eliminada correctamente");
        }catch (EntidadesException ex){
            return new respuestaDto("406", ex.getMessage());
        }
        
    }

    @GET
    @Path("{nit}")
    @Produces({MediaType.APPLICATION_JSON})
    public Empresa find(@PathParam("id") String id, @HeaderParam("auth") String authentication) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Empresa> findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {
        try {
            validadorEmpresa.validarAutenticacion(authentication, password);
            return super.findAll();
        } catch (EntidadesException ex) {
            Logger.getLogger(EmpresaFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Empresa> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
