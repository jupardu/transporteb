/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.controlador.servicios;

import co.gov.transporte.entidades.Conductor;
import co.gov.transporte.entidades.Paradero;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import transporte.dto.respuestaDto;
import transporte.entidades.excepciones.EntidadesException;
import transporte.entidades.validadores.ValidadorBuseta;
import transporte.entidades.validadores.validadorConductor;
import transporte.entidades.validadores.validadorParadero;

/**
 *
 * @author JULIAN
 */
@Stateless
@Path("co.gov.transporte.entidades.paradero")
public class ParaderoFacadeREST extends AbstractFacade<Paradero> {

    @PersistenceContext(unitName = "transporteBPU")
    private EntityManager em;

    public ParaderoFacadeREST() {
        super(Paradero.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto createParadero(Paradero entity, @HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password ) {
        try{
          validadorParadero.validarAutenticacion(authentication, password);
          validadorParadero.validarIdExistente(entity.getId(), this);
          validadorParadero.validarCamposObligatorios(entity);
            super.create(entity);
          return new respuestaDto("406","El paradero fue creado con exito");        
        }catch (EntidadesException ex){
          return new respuestaDto("406", ex.getMessage());
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto editParadero(Paradero entity) {
        
        try{
            validadorParadero.validarIdNoExistente(entity.getId(), this);
            validadorParadero.validarCamposObligatorios(entity);        
        super.edit(entity);
        return new respuestaDto("204","Paradero actualizado correctamente");
        } catch (EntidadesException ex) {
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @DELETE
    @Path("{id}")
    public respuestaDto removeConductor(@PathParam("id") String id) {
        try{
            validadorParadero.validarIdExistente(id, this);
            super.remove(super.find(id));
            return new respuestaDto("204","Paradero eliminado correctamente");
        }catch (EntidadesException ex){
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Paradero find(@PathParam("id") String id, @HeaderParam("auth") String authentication) {
        return super.find(id);
    }

    @GET    
    @Produces({MediaType.APPLICATION_JSON})
    public List<Paradero> findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {
        try {
            validadorParadero.validarAutenticacion(authentication, password);
            return super.findAll();
        } catch (EntidadesException ex) {
            Logger.getLogger(ParaderoFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Paradero> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
