/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.controlador.servicios;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author JULIAN
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(co.gov.transporte.controlador.servicios.BusetaFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.ConductorFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.EmpresaFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.ParaderoFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.RutaFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.UsuarioFacadeREST.class);
        resources.add(co.gov.transporte.controlador.servicios.ViajeFacadeREST.class);
    }
    
}
