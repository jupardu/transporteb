/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.controlador.servicios;

import co.gov.transporte.entidades.Buseta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import transporte.dto.respuestaDto;
import transporte.entidades.excepciones.EntidadesException;
import transporte.entidades.validadores.ValidadorBuseta;

/**
 *
 * @author JULIAN
 */
@Stateless
@Path("co.gov.transporte.entidades.buseta")
public class BusetaFacadeREST extends AbstractFacade<Buseta> {

    @PersistenceContext(unitName = "transporteBPU")
    private EntityManager em;

    public BusetaFacadeREST() {
        super(Buseta.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto createBuseta(Buseta entity, @HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password ) {
        try{
          ValidadorBuseta.validarAutenticacion(authentication, password);  
          ValidadorBuseta.validarPlacaExistente(entity.getPlaca(), this);
          ValidadorBuseta.validarCamposObligatorios(entity);
            super.create(entity);
          return new respuestaDto("406","La buseta ya existe");        
        }catch (EntidadesException ex){
          return new respuestaDto("406", ex.getMessage());
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto editBuseta(Buseta entity) {
        
        try{
            ValidadorBuseta.validarPlacaNoExistente(entity.getPlaca(), this);
            ValidadorBuseta.validarCamposObligatorios(entity);        
        super.edit(entity);
        return new respuestaDto("204","buseta actualizada correctamente");
        } catch (EntidadesException ex) {
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @DELETE
    @Path("{placa}")
    public respuestaDto removeBuseta(@PathParam("placa") String placa) {
        try{
            ValidadorBuseta.validarPlacaExistente(placa, this);
            super.remove(super.find(placa));
            return new respuestaDto("204","buseta eliminada correctamente");
        }catch (EntidadesException ex){
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @GET
    @Path("{placa}")
    @Produces({MediaType.APPLICATION_JSON})
    public Buseta find(@PathParam("id") String id, @HeaderParam("auth") String authentication) {
        
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Buseta> findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {
        try {
            ValidadorBuseta.validarAutenticacion(authentication, password);
            return super.findAll();
        } catch (EntidadesException ex) {
            Logger.getLogger(BusetaFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Buseta> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
