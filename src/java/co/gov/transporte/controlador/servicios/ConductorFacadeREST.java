/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.transporte.controlador.servicios;

import co.gov.transporte.entidades.Conductor;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import transporte.dto.respuestaDto;
import transporte.entidades.excepciones.EntidadesException;
import transporte.entidades.validadores.ValidadorBuseta;
import transporte.entidades.validadores.validadorConductor;

/**
 *
 * @author JULIAN
 */
@Stateless
@Path("co.gov.transporte.entidades.conductor")
public class ConductorFacadeREST extends AbstractFacade<Conductor> {

    @PersistenceContext(unitName = "transporteBPU")
    private EntityManager em;

    public ConductorFacadeREST() {
        super(Conductor.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto createConductor(Conductor entity, @HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password ) {
        try{
          validadorConductor.validarAutenticacion(authentication, password);  
          validadorConductor.validarConductorExistente(entity.getCedula(), this);
          validadorConductor.validarCamposObligatorios(entity);
            super.create(entity);
          return new respuestaDto("406","El conductor fue creado con exito");        
        }catch (EntidadesException ex){
          return new respuestaDto("406", ex.getMessage());
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public respuestaDto editConductor(Conductor entity) {
        
        try{
            validadorConductor.validarConductorNoExistente(entity.getCedula(), this);
            validadorConductor.validarCamposObligatorios(entity);        
        super.edit(entity);
        return new respuestaDto("204","Conductor actualizado correctamente");
        } catch (EntidadesException ex) {
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @DELETE
    @Path("{cedula}")
    public respuestaDto removeConductor(@PathParam("cedula") String cedula) {
        try{
            validadorConductor.validarConductorExistente(cedula, this);
            super.remove(super.find(cedula));
            return new respuestaDto("204","Conductor eliminado correctamente");
        }catch (EntidadesException ex){
            return new respuestaDto("406", ex.getMessage());
        }
    }

    @GET
    @Path("{cedula}")
    @Produces({MediaType.APPLICATION_JSON})
    public Conductor find(@PathParam("id") String id, @HeaderParam("auth") String authentication) {
        return super.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Conductor> findAll(@HeaderParam("auth") String authentication,
            @HeaderParam("passwd") String password) {
        try {
            validadorConductor.validarAutenticacion(authentication, password);
            return super.findAll();
        } catch (EntidadesException ex) {
            Logger.getLogger(ConductorFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Conductor> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
